# Value Iteration Gridmap

Reads PNG images and outputs value iteration PNG heat maps, numpy value array and json metadata

Requirements:
Python 3.6
PIL

#### Usage
```
python ValueIteration.py
```

For arguments
```
python MapGenerator.py -help
```

##### TODO
