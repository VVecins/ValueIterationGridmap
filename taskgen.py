from sklearn.model_selection import ParameterGrid
import subprocess
import traceback
import logging
import os
import re
import sys
import time
import copy
import json
import platform
import numpy as np
import math
import argparse
import subprocess

from datetime import datetime
from modules.logging_utils import LoggingUtils
from modules.args_utils import ArgsUtils
from modules.file_utils import FileUtils

parser = argparse.ArgumentParser(description='task generator (HPC)')

parser.add_argument('-name', default='', type=str)
parser.add_argument('-conda_env', help='name of conda environment', default='conda_env', type=str)

parser.add_argument("-output_directory", help="directory to write map images", default="valueMaps")
parser.add_argument("-input_directory", help="directory to read map images", default="maps")
parser.add_argument("-is_delete_previous_data", default=True, type=lambda x: (str(x).lower() == 'true'))

parser.add_argument("-threads_per_node", default=24, type=int)
parser.add_argument("-cpu_per_node", default=16, type=int)
parser.add_argument("-count_nodes", help='how many nodes to use', default=12, type=int)

args, args_other = parser.parse_known_args()

if len(args.name) == 0:
    args.name = datetime.now().strftime('%y-%m-%d_%H-%M-%S')

path_base = os.path.dirname(os.path.abspath(__file__))
print(path_base)

FileUtils.createDir(f'{path_base}/tasks')
FileUtils.createDir(f'{path_base}/tasks/{args.name}')
FileUtils.createDir(args.output_directory)

logging_utils = LoggingUtils(filename=os.path.join('tasks', args.name + '.txt'))
ArgsUtils.log_args(args, 'taskgen.py', logging_utils)


if args.is_delete_previous_data:
    FileUtils.deleteDir(args.output_directory, is_delete_dir_path=False)

count_maps = 0
for root, dirs, files in os.walk(args.input_directory):
    count_maps = len(files)

maps_per_node = int(math.floor(count_maps / args.count_nodes))

logging.info(f'{args.input_directory} contain {count_maps} maps')
logging.info(f'each of {args.count_nodes} nodes will process {maps_per_node}')

idx_from = -maps_per_node
idx_to = 0
for idx_node in range(0, args.count_nodes):

    idx_from += maps_per_node
    idx_to += maps_per_node

    task_name = f'{args.name}_{idx_node}'
    script_path = f'{path_base}/tasks/{args.name}/{task_name}.sh'

    with open(script_path, 'w') as fp:

        fp.write(f'#!/bin/sh -v\n')
        fp.write(f'#PBS -e {path_base}/tasks/{task_name}.e.txt\n')
        fp.write(f'#PBS -o {path_base}/tasks/{task_name}.txt\n')
        fp.write(f'#PBS -q rudens\n')
        fp.write(f'#PBS -l nodes=1:ppn={args.cpu_per_node}\n')
        fp.write(f'#PBS -l feature=centos7\n')

        fp.write(f'#PBS -l mem=64gb\n')
        fp.write(f'#PBS -l pmem=10gb\n')
        fp.write(f'#PBS -l walltime=128:00:00\n\n')

        fp.write(f'module load conda\n')
        fp.write(f'export TMPDIR=$HOME/tmp\n')
        fp.write(f'export SDL_AUDIODRIVER=waveout\n')
        fp.write(f'export SDL_VIDEODRIVER=x11\n')

        fp.write(f'cd {path_base}\n')
        fp.write(f'source activate {args.conda_env}\n')

        fp.write(f'python {path_base}/ValueIteration.py -threads {args.threads_per_node} -input_range {idx_from}:{idx_to} -is_delete_previous_data false -input_directory {args.input_directory} -output_directory {args.output_directory}\n')

    cmd = f'chmod +x {script_path}'
    logging.info(cmd)
    stdout = subprocess.check_output(cmd, shell=True, encoding='utf-8')
    logging.info(stdout)

    cmd = 'qsub -N ' + task_name + ' ' + script_path
    logging.info(cmd)
    stdout = subprocess.check_output(cmd, shell=True, encoding='utf-8')
    logging.info(stdout)

logging.info('all tasks started')