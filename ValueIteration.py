import argparse
from PIL import Image
import GridMap
import os
import traceback, sys
from multiprocessing import Queue, Process
import time

from modules.file_utils import FileUtils

parser = argparse.ArgumentParser()
parser.add_argument("-map_cell_size", help="cell size in pixels", default=20, type=int)
parser.add_argument("-epsilon", help="convergence value", default=1e-10, type=float)
parser.add_argument("-checkpoint_epsilon", help="delta step to save checkpoint", default=0.1, type=float)
parser.add_argument("-gamma", help="discount value", default=0.95, type=float)
parser.add_argument("-probability", help="stochastic 0 <= x < 1, deterministic = 1. main action probability", default=1, type=float)
parser.add_argument("-output_show_map", default=False, type=lambda x: (str(x).lower() == 'true'))
parser.add_argument("-sarsa_mode", default=True, type=lambda x: (str(x).lower() == 'true'))
parser.add_argument("-diagonal_movement", default=False, type=lambda x: (str(x).lower() == 'true'))

parser.add_argument("-output_directory", help="directory to write map images", default="valueMaps")
parser.add_argument("-input_directory", help="directory to read map images", default="maps")
parser.add_argument("-input_range", help="input index range: start:end(from start to end) or start: (from start to last) or :end(frm begining to end)", default="0:")
parser.add_argument("-is_delete_previous_data", default=True, type=lambda x: (str(x).lower() == 'true'))

parser.add_argument("-threads", default=1, type=int)

args, args_other = parser.parse_known_args()

def process_map(index):
    startTime = time.time()
    try:
        outputDir = os.path.join(args.output_directory, index[:-4])
        FileUtils.createDir(outputDir)
        filename = os.path.join(args.input_directory, index)

        myImage = Image.open(filename)
        inputMap = GridMap.GridMap(
            myImage,
            outputDir,
            args.map_cell_size,
            args.probability,
            diagonal_movement=args.diagonal_movement)

        inputMap.ParseImage(myImage)
        if args.output_show_map:
            myImage.show()

        inputMap.ValueIteration(
            is_output_show_map=args.output_show_map,
            is_sarsa=args.sarsa_mode,
            gamma=args.gamma,
            epsilon=args.epsilon,
            checkpointEpsilon=args.checkpoint_epsilon)

        del myImage
        del inputMap
        del outputDir
        del filename

    except Exception as e:
        print(f'error in file: {index}')
        print(str(e))
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))

    print(f'Task for {index} took: {time.time() - startTime}')

def worker(queue):
    while not queue.empty():
        idx = queue.get()
        process_map(idx)

if __name__ == '__main__':
    FileUtils.createDir(args.output_directory)
    if args.is_delete_previous_data:
        FileUtils.deleteDir(args.output_directory, is_delete_dir_path=False)

    start, end = args.input_range.split(":")

    # Create list of input images
    input_files = []
    input_files = os.listdir(args.input_directory)

    # parse input range
    if start != "":
        if int(start) > len(input_files):
            print(f'start index: {start} is bigger than file count {len(input_files)}')
            quit()
    if end != "":
        if int(end) > len(input_files):
            end = len(input_files)

    queue = Queue()
    if start == "":
        for i in range(int(end)):
            queue.put(input_files[i])
    elif end == "":
        for i in range(int(start), len(input_files)):
            queue.put(input_files[i])
    else:
        for i in range(int(start), int(end)):
            queue.put(input_files[i])

    start = time.time()

    processes = []
    for _ in range(args.threads):
        process = Process(target=worker, args=(queue,))
        process.daemon = True
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    print('Entire job took:', time.time() - start)
