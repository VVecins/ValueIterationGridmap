from PIL import Image, ImageDraw, ImageColor
import numpy as np
import os
import colorlover as cl
import json
import math

BLACK = [0, 0, 0]
WHITE = [255, 255, 255]
RED = [255, 0, 0]


class GridMap:

    def __init__(
            self,
            image,
            dir,
            step,
            probability,
            diagonal_movement = False,
            reward_wall = -10,
            reward_goal = 10,
            reward_hall = 0
    ):
        self.reward_wall = reward_wall
        self.reward_goal = reward_goal
        self.reward_hall = reward_hall

        # u - up, r - right, d - down, l - left
        if diagonal_movement:
            # 8 directional movement
            self.actions = ["u", "ur", "r", "rd", "d", "dl", "l", "lu"]
        else:
            # 4 directional movement
            self.actions = ["u", "r", "d", "l"]

        self.probability = probability
        self.image = Image.new(mode='RGB', size=(image.size[0], image.size[1]), color=255)
        self.imageOriginal = image
        self.outputDirectory = dir
        self.step = step
        self.cols = int(image.size[0] / self.step)
        self.rows = int(image.size[1] / self.step)
        self.startPoint = np.zeros(2)
        self.grid = np.zeros((self.cols, self.rows), dtype=object)
        # self.ParseImage(image)

    def ParseImage(self, image):
        for x in range(0, image.size[0], self.step):  # width cols
            for y in range(0, image.size[1], self.step):  # height rows
                pixel = image.getpixel((x + self.step / 2, y + self.step / 2))
                if list(pixel) == BLACK:
                    self.grid[int(x / self.step)][int(y / self.step)] = Point(x / self.step, y / self.step, "wall")
                elif list(pixel) == WHITE:
                    self.grid[int(x / self.step)][int(y / self.step)] = Point(x / self.step, y / self.step, "passage")
                elif list(pixel) == RED:
                    self.grid[int(x / self.step)][int(y / self.step)] = Point(x / self.step, y / self.step, "passage")
                    self.startPoint = [int(x / self.step), int(y / self.step)]
                else:
                    return 1

    # create gridmap sized array
    def GetInitialArray(self, dataType):
        array = np.zeros((self.cols, self.rows), dtype=dataType)
        return array

    def GetNeighbor(self, state, action, neighbor, rewards):
        newState = state
        probability = 0
        if neighbor == "u":
            if action == "u":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetRowNr() - 1 > 0:
                newState = self.grid[state.GetColNr()][state.GetRowNr() - 1]
        elif neighbor == "ur":
            if action == "ur":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetColNr() + 1 < self.cols - 1 and state.GetRowNr() - 1 > 0:
                newState = self.grid[state.GetColNr() + 1][state.GetRowNr() - 1]
        elif neighbor == "r":
            if action == "r":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetColNr() + 1 < self.cols - 1:
                newState = self.grid[state.GetColNr() + 1][state.GetRowNr()]
        elif neighbor == "rd":
            if action == "rd":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetColNr() + 1 < self.cols - 1 and state.GetRowNr() + 1 < self.rows - 1:
                newState = self.grid[state.GetColNr() + 1][state.GetRowNr() + 1]
        elif neighbor == "d":
            if action == "d":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetRowNr() + 1 < self.rows - 1:
                newState = self.grid[state.GetColNr()][state.GetRowNr() + 1]
        elif neighbor == "dl":
            if action == "dl":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetRowNr() + 1 < self.rows - 1 and state.GetColNr() - 1 > 0:
                newState = self.grid[state.GetColNr() - 1][state.GetRowNr() + 1]
        elif neighbor == "l":
            if action == "l":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetColNr() - 1 > 0:
                newState = self.grid[state.GetColNr() - 1][state.GetRowNr()]
        elif neighbor == "lu":
            if action == "lu":
                probability = self.probability + ((1 - self.probability) / len(self.actions))
            else:
                probability = (1 - self.probability) / len(self.actions)
            if state.GetColNr() - 1 > 0 and state.GetRowNr() - 1 > 0:
                newState = self.grid[state.GetColNr() - 1][state.GetRowNr() - 1]
        # self state reward
        reward = rewards[state.GetColNr()][state.GetRowNr()]
        return newState, probability, reward

    def ValueIteration(
            self,
            epsilon=1e-10,
            gamma=1,
            checkpointEpsilon=0.1,
            is_sarsa=False,
            is_output_show_map=False):

        self.is_output_show_map = is_output_show_map
        self.is_sarsa = is_sarsa

        lastDelta = np.inf
        jsonData = []
        # init value array and start + wall values
        policyValues = self.GetInitialArray("float")
        policyValues[self.startPoint[0]][self.startPoint[1]] = self.reward_goal
        self.grid[self.startPoint[0]][self.startPoint[1]].SetValue(self.reward_goal)
        # init reward array and start + wall values
        rewards = self.GetInitialArray("float")
        rewards[self.startPoint[0]][self.startPoint[1]] = self.reward_goal
        for x in range(self.cols):
            for y in range(self.rows):
                if self.grid[x][y].type == "wall":
                    policyValues[x][y] = self.reward_wall
                    self.grid[x][y].SetValue(self.reward_wall)
                    rewards[x][y] = self.reward_wall
        self.rewards = rewards

        idx_iter = 0
        imageName = os.path.join(self.outputDirectory, f"{idx_iter}.png")
        self.imageOriginal.save(imageName)

        memmapName = os.path.join(self.outputDirectory, f"{idx_iter}.memmap")
        fp = np.memmap(memmapName, dtype=np.float16, mode='w+', shape=(self.cols, self.rows))
        fp[:] = rewards[:]
        fp.flush()
        del fp

        # start value iteration
        while True:
            idx_iter += 1
            deltaValues = []
            maxValue = float('-Inf')
            minValue = float('+Inf')
            minValueTillWall = float('+Inf')
            for x in range(self.cols):
                for y in range(self.rows):
                    stateActions = []
                    originalValue = policyValues[x][y]
                    for action in self.actions:
                        actionSum = 0
                        for neighbor in self.actions:
                            newState, probability, reward = self.GetNeighbor(self.grid[x][y], action, neighbor, rewards)
                            actionSum += probability * (reward + (gamma * newState.value))
                        stateActions.append(actionSum)

                    policyValues[x][y] = max(stateActions)
                    # all elements in rewards are terminal states
                    if rewards[x][y] != 0: # change state value only if terminal reward is not known
                        policyValues[x][y] = rewards[x][y]

                    if not self.is_sarsa:
                        #non SARSA (used updated values)
                        self.grid[x][y].value = policyValues[x][y]

                    deltaValues.append(abs(originalValue - policyValues[x][y]))
            for x in range(self.cols):
                for y in range(self.rows):
                    self.grid[x][y].value = policyValues[x][y]
                    maxValue = max(maxValue, policyValues[x][y])
                    minValue = min(minValue, policyValues[x][y])
                    if policyValues[x][y] > self.reward_wall:
                        minValueTillWall = min(minValueTillWall, policyValues[x][y])
            if minValue == self.reward_wall:
                minValue = minValueTillWall
            delta = max(deltaValues)

            # save value iteration checkpoint data
            # print(f"convergence={delta} idx_iter={idx_iter}")
            is_checkpoint = False
            if lastDelta - delta > checkpointEpsilon or delta == 0:
                lastDelta = delta
                self.DrawValues()
                imageName = os.path.join(self.outputDirectory, f"{idx_iter}.png")
                self.image.save(imageName)
                memmapName = os.path.join(self.outputDirectory, f"{idx_iter}.memmap")

                fp = np.memmap(memmapName, dtype=np.float16, mode='w+', shape=(self.cols, self.rows))
                fp[:] = policyValues[:]
                fp.flush()
                del fp

                is_checkpoint = True

            # append json data each iteration
            jsonData.append(
                {
                    'iteration': idx_iter,
                    'convergenceDelta': delta,
                    'minValue': minValue,
                    'maxValue': maxValue,
                    'is_checkpoint': is_checkpoint
                })

            # check for convergence
            if delta < epsilon:
                self.DrawValues()
                imageName = os.path.join(self.outputDirectory, f"{idx_iter}.png")
                self.image.save(imageName)
                if self.is_output_show_map:
                    self.image.show()
                break

        jsonFileName = os.path.join(self.outputDirectory, "metadata.json")
        with open(jsonFileName, 'w') as outfile:
            json.dump({
                'memmap_shape': [self.cols, self.rows],
                'iterations': jsonData
            }, outfile, indent=4)
        return policyValues

    def DrawValues(self):
        maxValue = float('-Inf')
        minValue = float('+Inf')
        minValueTillWall = float('+Inf')

        for x in range(self.cols):
            for y in range(self.rows):
                value = self.grid[x][y].GetValue()
                maxValue = max(maxValue, value)
                minValue = min(minValue, value)
                if value > self.reward_wall:
                    minValueTillWall = min(minValueTillWall, value)

        if minValue == self.reward_wall:
            minValue = minValueTillWall

        color_scale = cl.scales['9']['div']['RdYlGn']
        color_bins_count = 50
        color_bins = cl.to_rgb(cl.interp(color_scale, color_bins_count))
        deltaValue = maxValue - minValue
        # print(f"minValue={minValue}, maxValue={maxValue}, deltaValue={deltaValue}")

        draw = ImageDraw.Draw(self.image)
        for x in range(self.cols):
            for y in range(self.rows):

                value = self.grid[x][y].GetValue()
                color = 'rgb(0,0,0)'
                if not self.rewards[x][y] == self.reward_wall:
                    color_idx = int(math.floor(((value - minValue) * (color_bins_count-1)) / deltaValue))
                    color = color_bins[color_idx]
                draw.rectangle(((self.grid[x][y].GetColNr() * self.step, self.grid[x][y].GetRowNr() * self.step),
                                (self.grid[x][y].GetColNr() * self.step + self.step,
                                 self.grid[x][y].GetRowNr() * self.step + self.step)), fill=color)

        del draw
        return self.image


class Point:

    def __init__(self,
                 col,
                 row,
                 pointType):
        self.col = int(col)
        self.row = int(row)
        self.type = pointType
        self.value = 0

    def SetValue(self, value):
        self.value = value

    def GetValue(self):
        return self.value

    def GetColNr(self):
        return self.col

    def GetRowNr(self):
        return self.row
