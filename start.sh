#!/bin/bash -v

module load conda
source activate conda_env
cd ~/Documents/VIN_Research_ValueIterationGridmap/

python3 ./taskgen.py -name maps128 -count_nodes 12 -is_delete_previous_data true -input_directory /mnt/home/evaldsu/data/maps128 -output_directory /mnt/home/evaldsu/data/vi_maps128
